package scanner;
import java_cup.runtime.Symbol;
import parser.sym;

%%

%class Scanner
%unicode
%cup
%line
%column
%public

%eofval{
    return new Symbol(sym.EOF);
%eofval}

%{   //codigo java para funcionar entrada via teclado. Explicação abaixo.
		public Scanner(java.io.InputStream in) {
				this(new java.io.InputStreamReader(in, java.nio.charset.Charset.forName("UTF-8")));
		}
%}


digito = [0-9]
letra = [a-zA-Z]
inteiro = {digito}+

fimdeLinha = \r|\n|\r\n
espaco = {fimdeLinha} | [ \t\f]
opMais = "+"

%%

{inteiro} { 
		int aux = Integer.parseInt (yytext());
		return new Symbol (sym.INTEIRO, new Integer(aux)); 
}

{opMais} { return new Symbol(sym.MAIS);	}

"-" { return new Symbol(sym.MENOS);}

";" { return new Symbol(sym.PTVIRG);}

{espaco} { /* despreza */ }

.|\n { /* Caractere inválido */ 		
	return new Symbol(sym.EOF, yyline, yycolumn, yytext());		
	}