
import parser.Parser;
import java.io.FileInputStream;
import scanner.Scanner;

public class App {

    public static void main(String[] args) throws Exception {
        FileInputStream in = null;
        try {
            //fileIn = new FileInputStream("teste.txt");
            //Scanner scanner = new Scanner(fileIn);

            Scanner scanner = new Scanner(System.in);
            Parser p = new Parser(scanner);
            p.parse();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
